<?php

use Illuminate\Database\Migrations\Migration;
use Jenssegers\Mongodb\Schema\Blueprint;

/**
 * Class CreateSepomexTable.
 */
class CreateSepomexTable extends Migration
{
    protected $connection = 'mongodb';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)
            ->table(config('sepomex.table_name'), function (Blueprint $collection) {
                $collection->unique('d_codigo');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)
            ->table(config('sepomex.table_name'), function (Blueprint $collection) {
                $collection->drop();
            });
    }
}
